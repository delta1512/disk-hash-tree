import os
import shutil
import unittest
import tempfile
from random import choice
from string import ascii_lowercase

from diskhashtree import DiskHashTree
from diskhashtree.test import util
from diskhashtree.test import data
from diskhashtree.test import DEFAULT_RANDOM_KEYS


N_RANDOM_KEYS = DEFAULT_RANDOM_KEYS


class TestAdd(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.dht = DiskHashTree(self.test_dir)

    def test_meta(self):
        '''Simply create the object and destroy it to test metadata init'''
        with open(os.path.join(self.test_dir, '.meta'), 'r', encoding='utf-8') as meta:
            self.assertTrue(bool(meta.read()))

    def test_root_dir_add(self):
        for k in data.TEST_KEYS_ROOT_DIR:
            self.dht.add(k)

        self.assertTrue(util.verify_dir(self.test_dir, data.TEST_KEYS_ROOT_DIR_STRUCT))

    def test_all_cases(self):
        for k in data.TEST_KEYS_ALL_CASES:
            self.dht.add(k)

        self.assertTrue(util.verify_dir(self.test_dir, data.TEST_KEYS_ALL_CASES_STRUCT))

    def test_random_add(self):
        n_keys = N_RANDOM_KEYS
        key_len = 8
        keys = set()

        for k in range(n_keys):
            current_key = ''
            for n in range(key_len):
                current_key += choice(ascii_lowercase)
            self.dht.add(current_key)

        # This is successful if there are no errors
        self.assertTrue(True)
    
    def tearDown(self):
        shutil.rmtree(self.test_dir)

import shutil
import unittest
import tempfile
from random import choice
from string import ascii_lowercase

from diskhashtree import DiskHashTree
from diskhashtree.test import util
from diskhashtree.test import data
from diskhashtree.test import DEFAULT_RANDOM_KEYS


N_RANDOM_KEYS = DEFAULT_RANDOM_KEYS


class TestIter(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.dht = DiskHashTree(self.test_dir)

    def test_fixed_iter(self):
        keys = data.TEST_KEYS_ALL_CASES
        for k in keys:
            self.dht.add(k)

        self.assertTrue(set(self.dht) == set(keys))

    def test_random_iter(self):
        n_keys = N_RANDOM_KEYS
        key_len = 8
        keys = set()

        for k in range(n_keys):
            current_key = ''
            for n in range(key_len):
                current_key += choice(ascii_lowercase)
            self.dht.add(current_key)
            keys.add(current_key)

        self.assertTrue(set(self.dht) == set(keys))

    def tearDown(self):
        shutil.rmtree(self.test_dir)

import shutil
import unittest
import tempfile

from diskhashtree import DiskHashTree
from diskhashtree.test import util
from diskhashtree.test import data


class TestIsEmpty(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.dht = DiskHashTree(self.test_dir)

    def test_is_empty(self):
        self.assertTrue(self.dht.is_empty())

    def test_not_empty_level_1(self):
        '''
        Tests for only files in the root dir
        '''
        for k in data.TEST_KEYS_ROOT_DIR:
            self.dht.add(k)

        self.assertFalse(self.dht.is_empty())

    def test_not_empty_level_2(self):
        '''
        Test for a single directory (2 level tree)
        '''
        self.dht.add('aaaaaaaa')
        self.dht.add('aabbbbbb')

        self.assertFalse(self.dht.is_empty())

    def test_add_remove_emptiness(self):
        '''
        Tests emptiness after keys were added and then removed
        '''
        for k in data.TEST_KEYS_ROOT_DIR:
            self.dht.add(k)

        for k in data.TEST_KEYS_ROOT_DIR:
            self.dht.remove(k)

        self.assertTrue(self.dht.is_empty())
    
    def tearDown(self):
        shutil.rmtree(self.test_dir)

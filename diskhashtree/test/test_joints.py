import shutil
import unittest
import tempfile

from diskhashtree import DiskHashTree
from diskhashtree.test import util
from diskhashtree.test import data


class TestJoints(unittest.TestCase):
    def setUp(self):
        self.test_dir = tempfile.mkdtemp()
        self.dht = DiskHashTree(self.test_dir)

    def test_is_subset(self):
        for k in data.TEST_KEYS_IS_SUBSET:
            self.dht.add(k)

        self.assertTrue(self.dht.issubset(data.TEST_IS_SUBSET))
        self.assertTrue(self.dht.issubset(data.TEST_IS_PROPER_SUBSET))
        self.assertFalse(self.dht.issubset(data.TEST_IS_NOT_SUBSET))

    def test_is_disjoint(self):
        for k in data.TEST_KEYS_IS_DISJOINT:
            self.dht.add(k)

        self.assertTrue(self.dht.isdisjoint(data.TEST_IS_DISJOINT))
        self.assertFalse(self.dht.isdisjoint(data.TEST_IS_NOT_DISJOINT))

    def test_bad_input(self):
        for k in data.TEST_KEYS_IS_DISJOINT:
            self.dht.add(k)

        self.assertTrue(self.dht.isdisjoint(data.TEST_BAD_SUBSET_DATA))
    
    def tearDown(self):
        shutil.rmtree(self.test_dir)

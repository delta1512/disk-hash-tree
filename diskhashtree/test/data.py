# For when a DiskHashTree is initialised, this should be the contents of the dir
EMPTY_STRUCT = ('.meta',)

# For testing files in the current directory
TEST_KEYS_ROOT_DIR = ('ou2ihoo8', 'aicee8ou', 'wai4me0i', 'coghaes4')
TEST_KEYS_ROOT_DIR_STRUCT = ('ou2ihoo8', 'aicee8ou', 'wai4me0i', 'coghaes4')

# For testing files with weird symbols
TEST_KEYS_DANGER_SYM = ('-----abcd', '-----abef', '-----bcde', '-----bcef')
TEST_KEYS_DANGER_SYM_STRUCT = (
    '-----ab',
    '-----ab/-----abcd',
    '-----ab/-----abef',
    '-----bc',
    '-----bc/-----bcde',
    '-----bc/-----bcef'
)

# This tests all cases for a large tree
TEST_KEYS_ALL_CASES = (
    'abcdef', 'abaaaa', # Test a level-1 collision
    'abeeee', # Ensure that the new file goes into the subdir
    'abaabb', # Test a level-2 collision
    'abaacc', # Ensure that new file goes into the farthest subtree
    'abcdef', # Test for a duplicate
    'zzzzzz' # Test a single item in the root
)
TEST_KEYS_ALL_CASES_STRUCT = (
    'ab', 'zzzzzz', # Level 1
    'ab/abcdef', 'ab/abeeee', 'ab/aa', # Level 2
    'ab/aa/abaaaa', 'ab/aa/abaabb', 'ab/aa/abaacc' # Level 3
)

# Some data to test for subsets
TEST_KEYS_IS_SUBSET = TEST_KEYS_ALL_CASES
TEST_IS_PROPER_SUBSET = {
    'zzzzzz', # Test element from an item in the root
    'abaaaa', # Test level 1 subtree
    'abaabb' # Test level 2 subtree
}
TEST_IS_NOT_SUBSET = {'yyyyyy', 'oooooo'}
TEST_IS_SUBSET = set(TEST_KEYS_ALL_CASES)

TEST_KEYS_IS_DISJOINT = TEST_KEYS_ALL_CASES
TEST_IS_DISJOINT = {'yyyyyy', 'oooooo', '123456'}
TEST_IS_NOT_DISJOINT = {'yyyyyy', 'oooooo', '123456', 'abaaaa'} # There is one intersecting element

# Weird inputs for the program. This should be disjoint to TEST_KEYS_IS_DISJOINT
TEST_BAD_SUBSET_DATA = {'k', '--------------17278', '-----abaaaa'}

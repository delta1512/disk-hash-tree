import os
import glob
import shutil


def verify_dir(directory: str, dir_struct: tuple):
    '''
    Verifies the tree structure of the DiskHashTree on-disk versus the expected
    tree structure. If the tree structure matches the expected structure, then
    this function returns true.

    :param directory: The root directory that the test is running in.

    :param dir_struct: A tuple that contains all of the files and subdirectories
        of the expected tree structure. The root directory (the first param)
        should be stripped from every member of the tuple.
    '''
    # Get the tree of the DiskHashTree on the file system
    tree_struct = glob.glob(os.path.join(directory, '**'), recursive=True)

    prefix = os.path.commonprefix(tree_struct)

    # Filter out irrelevant stuff and the root directory from every name
    clean_tree_struct = set(filter(bool, [d.replace(prefix, '') for d in tree_struct]))

    return set(dir_struct) == clean_tree_struct
